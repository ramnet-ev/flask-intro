#!/usr/bin/env python


def decoration(any_function):
    print("\n##### Starting #####\n")
    any_function()
    print("\n##### Stopping #####\n")


@decoration
def my_function():
    print("This is my_function() working.")
